﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HudController : MonoBehaviour {

    public static HudController Instance;

    public Text healthValue;

    void Awake()
    {
        if (HudController.Instance != null)
        {
            Destroy(gameObject);
        }

        HudController.Instance = this;
    }

    public void SetBaseHealth(int health)
    {
        healthValue.text = health >= 0 ? health.ToString() : "0";
    }
}
