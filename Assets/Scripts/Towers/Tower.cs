﻿using UnityEngine;
using System.Collections;
using System;

public class Tower : MonoBehaviour {

    public GameObject projectile;
    public float attackSpeed;
    public Vector2 projectileShootForce;
    public int experience;
    public AttributesPerLevel[] levelsAttributes;
    public int level { get; protected set; }
    public Transform fireAnchorPoint;
    public GameObject attackRangeReference;

    float _lastAttack = 0;

    void OnTriggerStay(Collider collider)
    {
        if (collider.CompareTag("Enemy"))
        {
            Attack(collider.gameObject);
        }
    }

    void Attack(GameObject target)
    {
        if (Time.time - _lastAttack > attackSpeed)
        {
            _lastAttack = Time.time;
            ShootProjectile(target);
        }
    }

    protected virtual void ShootProjectile(GameObject target)
    {
        GameObject newProjectile = GameObject.Instantiate(projectile);
        newProjectile.GetComponent<Projectile>().Target = target.transform;
        newProjectile.transform.position = fireAnchorPoint.position;
        newProjectile.GetComponent<Projectile>().moveSpeed = projectileShootForce.x;

		int pathPoints = 20;
		Vector3[] positions = new Vector3[pathPoints];
        
		for (int i = 0; i < pathPoints; i++)
        {
			positions[i] = CalculateBezierPoint(i / (float)pathPoints, newProjectile.transform.position, 
				newProjectile.transform.position + new Vector3(target.transform.position.x - gameObject.transform.position.x, projectileShootForce.y, target.transform.position.z - gameObject.transform.position.z), 
				target.transform.position, 
				target.transform.position);
        }

        newProjectile.GetComponent<Projectile>().path = positions;
    }

    Vector3 CalculateBezierPoint(float t, Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3)
    {
        float u = 1f - t;
        float tt = t * t;
        float uu = u * u;
        float uuu = uu * u;
        float ttt = tt * t;

        Vector3 p = uuu * p0;
        p += 3 * uu * t * p1;
        p += 3 * u * tt * p2;
        p += ttt * p3;

        return p;
    }

    [Serializable]
    public struct AttributesPerLevel
    {
        public int neededExperience;
        public int extraDamage;
        public int extraAttackSpeed;
    }
}
