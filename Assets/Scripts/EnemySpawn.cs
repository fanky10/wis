﻿using UnityEngine;
using System.Collections;
using System;

public class EnemySpawn : MonoBehaviour {

	public GameObject enemyPrefab;
	public float interval = 3f;

	public Wave[] waves;

	// Use this for initialization
	void Start () {
		InvokeRepeating ("SpawnWave",interval,interval);
	}

	void SpawnWave()
	{
		Instantiate (enemyPrefab, transform.position, Quaternion.identity);
	}

	[Serializable]
	public struct Wave
	{
		public SubWave[] enemyType;
	}

	[Serializable]
	public struct SubWave
	{
		public GameObject enemyPrefab;
		public int qty;
	}
}


