﻿using UnityEngine;
using System.Collections;

public class Arrow : Projectile {

	Vector3 lastKnownPosition;

    void FixedUpdate()
    {
        if (path != null)
        {
			lastKnownPosition = target != null ? target.position : lastKnownPosition;

			Vector3 displacement = lastKnownPosition - originalTargetPosition;

			if (currentPathIndex >= 0 && currentPathIndex < path.Length)
            {
                Vector3 arrowToEnemy = path[currentPathIndex] - transform.position + displacement;
                Quaternion newRotation = Quaternion.LookRotation(arrowToEnemy);
                gameObject.GetComponent<Rigidbody>().MoveRotation(Quaternion.Lerp(gameObject.transform.rotation, newRotation, 30 * Time.deltaTime));
            }

            gameObject.GetComponent<Rigidbody>().MovePosition(gameObject.transform.position + gameObject.transform.forward * moveSpeed * Time.deltaTime);

			if (currentPathIndex >= 0 && Vector3.Distance(gameObject.transform.position, path[currentPathIndex] + displacement) < 0.5f)
            {
                currentPathIndex = currentPathIndex + 1 >= path.Length ? -1 : currentPathIndex + 1;
            }
        }
    }
}
