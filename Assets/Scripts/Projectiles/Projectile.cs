﻿using UnityEngine;
using System.Collections;

public abstract class Projectile : MonoBehaviour {
    public int damage;
    public float damageRadius;
    public Vector3[] path;
    public float moveSpeed;
    public Tower parentTower;
    public Vector3 originalTargetPosition; 

    protected Transform target;
    protected int currentPathIndex = 1;

    public Transform Target
    {
        get { return target; }
        set
        {
            target = value;
            originalTargetPosition = target.position;
        }
    }

    public void OnCollisionEnter(Collision collision)
    {
		IDamageable damageable = collision.gameObject.GetComponent<IDamageable> ();

		if (damageable != null) {
			damageable.TakeDamage (damage);
		}

        Destroy(gameObject);
        currentPathIndex = 1;
    }
}
