﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour, IDamageable{

    public int healthPoints;
    public int armorPoints;
    public int gold;
    public int experiencePoints;
    public int damage;

    int _originalHealthPoints;

    void Awake()
    {
		_originalHealthPoints = healthPoints;
        Reset();
    }

    public void TakeDamage(int damage)
    {
		int trueDamage = damage - armorPoints;
		healthPoints -= trueDamage > 0 ? trueDamage : 0;
        if (healthPoints <= 0)
        {
            Die();
        }
    }

    public void Die()
    {
        GetComponent<EnemyMovement>().enabled = false;

        //Replace this by returning the object to the pool
		Destroy (this.gameObject);
    }

    public void Reset()
    {
        healthPoints = _originalHealthPoints;
        GetComponent<EnemyMovement>().enabled = true;
    }
}
