﻿using UnityEngine;
using System.Collections;

public class EnemyMovement : MonoBehaviour {

	Transform playerBase;
	NavMeshAgent navAgent;

	void Awake()
	{
		playerBase = GameObject.FindGameObjectWithTag ("PlayerBase").transform;
		navAgent = GetComponent <NavMeshAgent> ();
	}

	// Use this for initialization
	void Start () {
		navAgent.destination = playerBase.position;
	}
}
