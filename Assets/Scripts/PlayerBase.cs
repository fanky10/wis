﻿using UnityEngine;
using System.Collections;

public class PlayerBase : MonoBehaviour, IDamageable {

    public int startingHealthPoints;

    int _currentHealthPoints;

    void Awake()
    {
        _currentHealthPoints = startingHealthPoints;
    }

    public void TakeDamage(int damage)
    {
        _currentHealthPoints -= damage;

        HudController.Instance.SetBaseHealth(_currentHealthPoints);
        if (_currentHealthPoints <= 0)
        {
            Die();
        }
    }

    public void Die()
    {
        //TODO Show lost message
        Time.timeScale = 0;
    }

    void OnTriggerEnter(Collider collider)
    {
        if (collider.CompareTag("Enemy"))
        {
            TakeDamage(collider.GetComponent<Enemy>().damage);
            collider.GetComponent<Enemy>().Die();
        }
    }
}
